#!/usr/bin/env python
# -*- coding: utf-8 -*-

import kivy
kivy.require('1.8.0')

from codeinputdemo.codeinputdemoapp import CodeinputdemoApp

if __name__ == "__main__":
    CodeinputdemoApp().run()
