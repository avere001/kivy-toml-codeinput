from kivy.app import App
from kivy.uix.codeinput import CodeInput
from pygments.lexers import get_lexer_by_name
from requests import get
from tomlkit import dumps
from tomlkit import parse
#from tomlkit import document

class CodeinputdemoApp(App):
    """Basic Kivy CodeInput example
    """

    def build(self):
        response = get("https://raw.githubusercontent.com/toml-lang/toml/master/tests/example.toml")
        toml_text = response.text
        
        # see https://github.com/sdispater/tomlkit a more comprehensive example
        data = parse(toml_text)  # or document() for an empty "dictionary" that preserves the order of elements
        del data["products"]

        data["my_new_table"] = {'a': [1,2,3]}
        data["my_new_table"]["subtable"] = {"arg": "pirates"}

        self.root.text = dumps(data)

        return self.root

