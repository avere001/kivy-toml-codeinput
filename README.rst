=============================
CodeInputDemo
=============================

CodeInput demo

download this example with git:
``git clone git@bitbucket.org:avere001/kivy-toml-codeinput.git``

Usage
-----

Using pipenv 
::
    pipenv install
    pipenv shell
    python main.py

Using pip  
::
    pip install -r requirements.txt
    python main.py
